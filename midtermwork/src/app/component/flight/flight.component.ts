import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { Flight } from './flight';
import { ex } from 'src/app/ex/ex';



@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  flights !: Flight[];
  flightForm !: FormGroup;
  defaultMinDate!: Date;

  constructor(private fb: FormBuilder, private example: ex) { }


  ngOnInit(): void {
    this.flightForm = this.fb.group({
      fullName: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      type: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['', Validators.required],
      adults: ['', [Validators.max(10), Validators.required]],
      children: ['', [Validators.max(10), Validators.required]],
      infants: ['', [Validators.max(10), Validators.required]]
    });
    this.defaultMinDate = new Date;
    this.getPages();
  }

  getPages() {
    this.flights = this.example.getPages();
  }

  onSubmit(f: Flight): void {
    const departureYear = f.departure.getFullYear() + 543;
    const arrivalYear = f.arrival.getFullYear() + 543;
    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + '/'  + departureYear);
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + '/' + arrivalYear);
    this.example.addFlight(f);
    this.flightForm.reset();
  }

}
