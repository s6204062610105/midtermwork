import { Injectable } from '@angular/core';
import { Flight } from '../component/flight/flight';
import { ex } from '../ex/ex';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] =[];

  constructor() {
    this.flights = ex.mflights
  }

  getPages():Flight[]{
    console.log(this.flights);
    return this.flights;
  }

  addFlight(f:Flight):void{
    this.flights.push(f);

  }
}

